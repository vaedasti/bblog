<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Inertia\Inertia;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = Post::with('author')
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->paginate(5);

        return Inertia::render('Blog/Index', [
            'posts' => $posts,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Blog/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request): RedirectResponse
    {
        // the incoming request is valid...

        // retrieve the validated input data...
        $validated = $request->validated();

        // if image presented
        if ($request->hasFile('image')) {
            // get file
            $file = $request->file('image');

            // upload photo
            $path = $this->upload_image(
                $file
            );

            // set image path
            $validated['image'] = Str::replaceFirst('public', '', $path);
        }

        // create post
        auth()->user()->posts()->create($validated);

        // redirect to dashboard
        return redirect(route('dashboard'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        return Inertia::render('Blog/Show', [
            'post' => $post,
            'author' => $post->author,
            'comments' => Comment::with('author')->where('post_id', $post->id)->orderBy('created_at', 'desc')->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        return Inertia::render('Blog/Edit', [
            'post' => $post,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePostRequest $request, string $slug)
    {
        // the incoming request is valid...

        // retrieve the validated input data...
        $validated = $request->validated();

        // get post
        $post = Post::where('slug', $slug)->firstOrFail();

        // if image presented
        if ($request->hasFile('image')) {
            // get file
            $file = $request->file('image');

            // upload image
            $path = $this->upload_image(
                $file
            );

            // if there already image
            if ($post->image) {
                //  delete old image
                $this->delete_image($post->image);
            }

            // set image path
            $validated['image'] = Str::replaceFirst('public', '', $path);
        }

        // update post
        $post->update($validated);

        // redirect to dashboard
        return redirect(route('dashboard'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $post = Post::findOrFail($id);

        // if user can delete that post then process
        if (Gate::check('delete', $post)) {
            // if post has image
            if ($post->image) {
                // delete image with parent directory
                $this->delete_image(
                    // directory
                    Str::of($post->image)->dirname()
                );
            }

            // delete post
            $post->delete();
        }

        return back();
    }

    // store file
    public function upload_image($file, string $filename = null, string $path = null)
    {
        // get param, if not presented get file hash
        $filename = $filename ?? $file->hashName();

        // get param, if not presented generate path with timestamp
        // add prefix public. it's root directory
        $path = 'public' . ($path ?? ('/images/blog/' . now()->getTimestamp()));

        // upload photo
        $uploaded_file_path = $file
            ->storeAs(
                $path,
                $filename
            );

        return $uploaded_file_path;
    }

    // delete image with parent directory
    public function delete_image(string $file_path): bool
    {
        if ($file_path) {
            // add prefix public. it's root directory
            return \Illuminate\Support\Facades\Storage::deleteDirectory('public' . $file_path);
        }

        return false;
    }
}
