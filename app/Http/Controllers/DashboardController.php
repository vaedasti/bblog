<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        $is_user_admin = auth()->user()->isAdmin();

        if ($is_user_admin) {
            // authenticated user is admin, show all posts
            $posts = Post::with('author')
                ->orderBy('created_at', 'desc')
                ->orderBy('id', 'desc')
                ->paginate(10);
        } else {
            // not admin, show only users posts
            $posts = auth()
                ->user()
                ->posts()
                ->orderBy('created_at', 'desc')
                ->orderBy('id', 'desc')
                ->paginate(10);
        }

        return Inertia::render('Dashboard', [
            'posts' => $posts,
            'is_user_admin' => $is_user_admin
        ]);
    }
}
