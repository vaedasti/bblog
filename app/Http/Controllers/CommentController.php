<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCommentRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Support\Facades\Gate;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCommentRequest $request, Post $post)
    {
        // the incoming request is valid...

        // retrieve the validated input data...
        $validated = $request->validated();

        $comment = new Comment;

        $comment->content = $validated['content'];
        $comment->post_id = $post->id;
        $comment->user_id = auth()->user()->id;

        $comment->save();

        // redirect to dashboard
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        // if user can delete that comment then process
        if (Gate::check('delete', $comment)) {
            // delete comment
            $comment->delete();
        }

        return back();
    }
}
