<?php

namespace App\Http\Middleware;

use App\Policies\RolePolicy;
use Illuminate\Http\Request;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     */
    public function version(Request $request): string|null
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @return array<string, mixed>
     */
    public function share(Request $request): array
    {
        return array_merge(parent::share($request), [
            'auth' => [
                'user' => $request->user(),
                'user_is_admin' => $request->user() ? $request->user()->isAdmin() : false,
                'can' => [
                    'viewAny' => [
                        'role' => (new RolePolicy)->viewAny($request->user),
                    ],
                    'view' => [
                        'role' => (new RolePolicy)->view($request->user),
                    ],
                    'create' => [
                        'role' => (new RolePolicy)->create($request->user),
                    ],
                    'update' => [
                        'role' => (new RolePolicy)->update($request->user),
                    ],
                    'delete' => [
                        'role' => (new RolePolicy)->delete($request->user),
                    ],
                    // 'restore' => [
                    //     'role' => (new RolePolicy)->restore($request->user),
                    // ],
                    // 'forceDelete' => [
                    //     'role' => (new RolePolicy)->forceDelete($request->user),
                    // ]
                ]
            ],
            'ziggy' => function () use ($request) {
                return array_merge((new Ziggy)->toArray(), [
                    'location' => $request->url(),
                ]);
            },
        ]);
    }
}
