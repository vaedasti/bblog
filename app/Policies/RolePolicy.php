<?php

namespace App\Policies;

use App\Models\Role;
// use App\Models\User;
// use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Gate;

class RolePolicy
{
    /**
     * Determine whether the user can view any roles.
     */
    public function viewAny(): bool
    {
        // is user has 'superadmin' or 'admin' role
        return Auth::user() ? Auth::user()->isAdmin() : false;
    }

    /**
     * Determine whether the user can view the role.
     */
    public function view(): bool
    {
        return $this->viewAny();
    }

    /**
     * Determine whether the user can create role.
     */
    public function create(): bool
    {
        return $this->viewAny();
    }

    /**
     * Determine whether the user can update the role.
     */
    public function update(): bool
    {
        return $this->viewAny();
    }

    /**
     * Determine whether the user can delete the role.
     */
    public function delete(): bool
    {
        return $this->viewAny();
    }

    /**
     * Determine whether the user can restore the model.
     */
    // public function restore(): bool
    // {
    //     // is user has 'superadmin' role
    //     return Auth::user()->roles()->find(Role::where('name', 'superadmin')->first()) ? true : false;
    // }

    /**
     * Determine whether the user can permanently delete the model.
     */
    // public function forceDelete(): bool
    // {
    //     // is user has 'superadmin' role
    //     return Auth::user()->roles()->find(Role::where('name', 'superadmin')->first()) ? true : false;
    // }
}
