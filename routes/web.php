<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('home');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/blog', [PostController::class, 'index'])->name('blog');
    Route::get('/blog/{slug}', [PostController::class, 'show'])->name('blog.show');

    Route::prefix('dashboard')->group(function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

        // post
        Route::get('/post/create', [PostController::class, 'create'])->name('dashboard.post.create');
        Route::post('/post/create', [PostController::class, 'store'])->name('dashboard.post.create.submit');

        Route::get('/post/edit/{slug}', [PostController::class, 'edit'])->name('dashboard.post.edit');
        Route::post('/post/edit/{slug}', [PostController::class, 'update'])->name('dashboard.post.edit.submit');

        Route::delete('/post/{id}', [PostController::class, 'destroy'])->name('dashboard.post.destroy');

        // comment
        Route::post('/comment/create/{post}', [CommentController::class, 'store'])->name('dashboard.comment.create.submit');
        Route::delete('/comment/{comment}', [CommentController::class, 'destroy'])->name('dashboard.comment.destroy');
    });
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
