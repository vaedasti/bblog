<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'slug' => \Illuminate\Support\Str::slug(fake()->sentence()),
            'title' => fake()->sentence(),
            'content' => fake()->paragraphs(fake()->randomDigitNotZero(), true),
            'image' => fake()->imageUrl(),
            'user_id' => \App\Models\User::first()->id,
        ];
    }
}
